//***************************
//***   Made by Mr-Cas    ***
//***************************

/**
 * Thread sleep simulation.
 * @param {number} milliseconds
 */
function sleep(milliseconds) 
{
	var start = new Date().getTime();
	while(true) 
	{
		if((new Date().getTime() - start) > milliseconds)
		return;
	}
}

/**
 * This function creates classic while loop but leaves
 * working time for UI thread.
 * To break loop the callback_iteration should return something 
 * (besides undefined ofcourse).
 *
 * @param {function} callback_expression
 * @param {function} callback_iteration
 * @param {function} callback_finished
 */
function whileUI(callback_expression, callback_iteration, callback_finished)
{
	var handler =
		function()
		{
			if(!callback_expression() || callback_iteration())
			{
				if(callback_finished)
					callback_finished();
				
				return;
			}
			
			setTimeout(handler);
		}
	
	handler();
}

/**
 * This function creates classic for loop but leaves
 * working time for UI thread.
 * To break loop the callback_iteration should return something 
 * (besides undefined ofcourse).
 *
 * @param {number} start
 * @param {number} end
 * @param {function} callback_iteration
 * @param {function} callback_finished
 */
function forUI(start, end, callback_iteration, callback_finished)
{	
	var index = start;
	var handler =
		function()
		{
			if(index >= end || callback_iteration(index))
			{
				if(callback_finished)
					callback_finished();
				
				return;
			}
			
			++index;
			
			setTimeout(handler);
		}
	
	handler();
}