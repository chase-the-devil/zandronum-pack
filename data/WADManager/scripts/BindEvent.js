/**
 * @param {HTMLElement} element
 * @param {string} eventName
 * @param {function} eventHandler
 */
function BindEvent(element, eventName, eventHandler) 
{
    if(element.addEventListener)
        element.addEventListener(eventName, eventHandler, false);
    else if(element.attachEvent) 
    {
        switch(eventName) 
        {
            case "wheel":
                element.attachEvent("onmousewheel", function()
	            {
	            	var event = window.event;
	            	event["deltaY"] = window.event.wheelDelta;
	            	
	                eventHandler(event);
	            });
            default:
                element.attachEvent("on" + eventName, function()
	            {
	                eventHandler(window.event);
	            });
        }
    }
}