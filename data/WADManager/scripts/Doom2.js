//***************************
//***   Made by Mr-Cas    ***
//***************************

//!!!
//TODO: Optimize ini mapping file (reverse key and value and put all in global section)
//!!!

//
//.pk3 - 50 4B 05 06 {archive}
//.wad - 49 57 41 44 {Internal where all the data}; 50 57 41 44 {Patch where all the data}
//
//typedef
//	struct FILE_ENTRY
//		{
//			unsigned __int32 	offData;
//			unsigned __int32	lenData;
//			byte				name[8];
//		}
//		
//	sFILE_ENTRY;
//
//typedef
//	sruct WAD
//		{
//			unsigned __int32	sig;			[PWAD_SIG, IWAD_SIG]
//			unsigned __int32 	numFiles;		[custom]
//			unsigned __int32 	offFAT;			[12]
//			
//			sFILE_ENTRY*		directory;
//		}
//		
//	sWAD;
//

//***		#ref <ADOBinaryStreamManager.js>
//***		#ref <Ini.js>
//***		#ref <Path.js>
//***		#ref <Threading.js>

//***		signatures
var PWAD_SIG = 0x44415750;	//Patch Where All the Data
var IWAD_SIG = 0x44415749;	//Internal Where All the Data
var PK_SIG = 0x06054B50;	//Package (Zip, Rar, pk3)

/**
 * @param {string} name
 */
function wadNameBytesFromString(name)
{
	var bytes = [];
	var length = name.length;
	var index = 0;
	
	if(length > 8)
		length = 8;
	
	while(index < length)
	{
		bytes.push(name.charCodeAt(index));
		++index;
	}
	
	while(index < 8)
	{
		bytes.push(0);
		++index;
	}
	
	return bytes;
}

/**
 * @typedef {{path :string, sig :number, data :object, manager :ADOBinaryStreamManager}} Stream
 *
 * Reading stream from file. 
 * If file is not Addon - returns undefined.
 *
 * @param {string} path
 * @returns {Stream}
 */
function openStream(path)
{
	var manager = new ADOBinaryStreamManager();
	var bStream = manager.openBinary(path);
	
	var bytes = manager.readBinary(bStream, 4);
	
	var stream = undefined;
	
	if(bytes.length == 4)
	{
		stream =
		{
			path : path,
			sig : ADOBinaryStreamManager.bytesToInt32(bytes),
			data : bStream,
			manager : manager
		};
	}
	
	return stream;
}

/**
 * @param {Stream} stream
 */
function closeStream(stream)
{
	stream.manager.closeBinary(stream.data);
	stream.manager = undefined;
}

/**
 * @typedef {{offData :number, lenData :number, name :string}} FileEntry
 * @typedef {{path : string, sig : number, numFiles : number, offFAT : number, directory : FileEntry[]}} sWAD
 *
 * Parsing WAD addon and closing input stream. 
 * If stream is not WAD - returns undefined
 * (input stream is still opened).
 *
 * @param {Stream} stream
 * @returns {sWAD}
 */
function parseWADObject(stream)
{
	if(stream.sig == IWAD_SIG || stream.sig == PWAD_SIG)
	{
		var sWAD = 
		{
			path : stream.path,
			sig : stream.sig,
			numFiles : undefined,
			offFAT : undefined,
			directory : []
		}
		
		bytes = stream.manager.readBinary(stream.data, 4);
		sWAD.numFiles = ADOBinaryStreamManager.bytesToInt32(bytes);
		
		bytes = stream.manager.readBinary(stream.data, 4);
		sWAD.offFAT = ADOBinaryStreamManager.bytesToInt32(bytes);
		
		closeStream(stream);
		return sWAD;
	}

	return undefined;
}

/**
 * For now it'll just check PK signature.
 * @param {Stream} stream
 * @returns {boolean}
 */
function parsePKObject(stream)
{
	if(stream.sig == PK_SIG)
		return true;
	
	return false;
}

/**
 * Reading information about 
 * directory of WAD addon by sWAD object.
 * (Reading processing from hard drive, 
 * so if file already does not exist - it'll fail)
 *
 * @param {sWAD} wadObject
 */
function parseWADDirectory(wadObject, progressCallback, finishedCallback)
{
	var manager = new ADOBinaryStreamManager();
	var bStream = manager.openBinary(wadObject.path);
	
	bStream.Position = wadObject.offFAT;
			
	var filesCount = wadObject.numFiles;
	var percentWeight = filesCount / 100;
	
	forUI(0, filesCount, function(index)
	{
		if(progressCallback)
			progressCallback(index / percentWeight);
	
		var fileEntry = 
		{
			offData : undefined,
			lenData	: undefined,
			name : undefined
		};
	
		bytes = manager.readBinary(bStream, 4);
		fileEntry.offData = ADOBinaryStreamManager.bytesToInt32(bytes);
		
		bytes = manager.readBinary(bStream, 4);
		fileEntry.lenData = ADOBinaryStreamManager.bytesToInt32(bytes);
		
		fileEntry.name = ADOBinaryStreamManager.stringFromUTF8Array(manager.readBinary(bStream, 8));
		
		wadObject.directory.push(fileEntry);
	},
	function()
	{
		manager.closeBinary(bStream);
		
		if(finishedCallback)
			finishedCallback();
	});
}

/**
 * Releasing wad object.
 *
 * @param {sWAD} wadObject
 */
function closeWADObject(wadObject)
{
	wadObject.directory = [];
	wadObject = undefined;
}

/**
 * Extract all entries from WAD into a folder.
 * After extracting there are 
 * are files created: enties.ini and each file for each entrie with index name.
 * File entries.ini is a name mapping file,
 * which showes real entrie name and its index.
 *
 * @param {sWAD} wadObject
 * @param {string} path
 */
function extractWAD(wadObject, path, progressCallback, finishedCallback)
{
	path = fixFolderPath(path);
		
	var ini = 
	{
		global : {}, 
		sections : 
		{
			items : {}
		}
	};	
	
	var manager = new ADOBinaryStreamManager();
	var bStream = manager.openBinary(wadObject.path);
		
	var length = wadObject.directory.length;
	var percentWeight = length / 100;
	
	forUI(0, length, function(index)
	{
		if(progressCallback)
			progressCallback(index / percentWeight);
		
		var entry = wadObject.directory[index];
		var key = index.toString();
		var section = index.toString();
		
		ini.sections.items[section] = 
		{
			items : {}
		};
		
		ini.sections.items[section].items[entry.name] = key;
		
		bStream.Position = entry.offData;
		
		var bEntryStream = manager.createBinary();
		
		manager.writeBinary(bEntryStream, manager.readBinary(bStream, entry.lenData));
		manager.saveBinary(bEntryStream, path + key);
		
		manager.closeBinary(bEntryStream);
	},
	function()
	{
		manager.closeBinary(bStream);
	
		writeIni(path + "entries.ini", ini);
		
		if(finishedCallback)
				finishedCallback();
	});
}

/**
 * @param {string} iniPath
 * @param {string} wadPath
 * @param {number} sig
 */
function packWAD(iniPath, wadPath, sig, progressCallback, finishedCallback)
{
	if(sig != IWAD_SIG && sig != PWAD_SIG)
		return;
	
	var ini = readIni(iniPath);
	
	iniPath = fixFolderPath(getParentFolder(iniPath));
	
	var manager = new ADOBinaryStreamManager();
	var bStream = manager.createBinary();
	
	var count = ini.sections.count;
	
	//sig
	manager.writeBinary(bStream, ADOBinaryStreamManager.int32ToBytes(sig));
	
	//numFiles
	manager.writeBinary(bStream, ADOBinaryStreamManager.int32ToBytes(count));
	
	//offFAT
	manager.writeBinary(bStream, [12, 0, 0, 0]);
	
	var sections = ini.sections.items;
	
	//sig + numFiles + offFAT + (count * sizeof(FILE_ENTRY))
	var baseDataOffset = 12 + (count << 4);
	
	var index = 0;
	var section = sections[index.toString()];
	var fso = new ActiveXObject("Scripting.FileSystemObject");
	var percentWeight = count / 100;
	
	whileUI(function()
	{
		return section;
	},
	//directory write
	function()
	{
		if(progressCallback)
			progressCallback(0, index / percentWeight);
		
		var values = section.items;
		
		for(var key in values)
		{
			var size = fso.GetFile(iniPath + values[key]).Size;
			
			//offData
			manager.writeBinary(bStream, ADOBinaryStreamManager.int32ToBytes(baseDataOffset));
			
			//lenData
			manager.writeBinary(bStream, ADOBinaryStreamManager.int32ToBytes(size));
			
			//name
			manager.writeBinary(bStream, wadNameBytesFromString(key));
			
			baseDataOffset += size;
			
			break;
		}
		
		section = sections[(++index).toString()];
	},
	function()
	{
		fso = undefined;
		index = 0;
		section = sections[index.toString()];
		
		whileUI(function()
		{
			return section;
		},
		//raw write
		function()
		{
			if(progressCallback)
				progressCallback(1, index / percentWeight);
			
			var values = section.items;
			
			for(var key in values)
			{
				var entry = manager.openBinary(iniPath + values[key]);
						
				manager.writeBinary(bStream, manager.readBinary(entry, entry.Size));
				
				manager.closeBinary(entry);
				
				break;
			}
			
			section = sections[(++index).toString()];
		},
		function()
		{
			manager.saveBinary(bStream, wadPath);
			manager.closeBinary(bStream);
			
			if(finishedCallback)
				finishedCallback();
		});
	});
}