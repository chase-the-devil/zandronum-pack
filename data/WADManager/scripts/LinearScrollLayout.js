﻿//***************************
//***   Made by Mr-Cas    ***
//***************************

//*** ref <GetEventTarget.js>
//*** ref <BindEvent.js.js>

/**
 * Layout for scrolling elements list.
 * 
 * @param {string} inId
 * @param {number} inVisibleLength
 * @param {HTMLElement[]} [inContent]
 * @returns {LinearScrollList}
 */
function LinearScrollList(inVisibleLength, inId, inContent)
{
    var _htmlList           = document.createElement("ul");
    var _visibleLength      = inVisibleLength;
    var _currPosition       = 0;
    var _selectedItem       = undefined;

    /**
     * @type {HTMLElement}
     */
    var _parent             = undefined;

    /**
     * @type {LinearScrollList}
     */
    var _LinearScrollList   = this;
    var _maxPosition        = 0;

    /**
     * @type {HTMLElement[]}
     */
    var _content = undefined;

    if(inContent)
        _content = inContent;
    else
        _content = [];

    //***       custom handlers
    //***       (only those ones, which cover default hnadlers)

    /**
     * @typedef {function(Event, HTMLElement) => any} EventHandler
     * @type {{wheel :EventHandler, mousedown : EventHandler, mouseleave :EventHandler, mouseup :EventHandler, item_selected :EventHandler, item_unselected :EventHandler}}
     */
    var _custom_event_handlers =
    {
        wheel : undefined,
        mousedown : undefined,
        mouseup : undefined,
        item_selected : undefined,
        item_unselected : undefined
    };

    //***

    function updateRenderList()
    {
        _htmlList.innerHTML = "";

        var listItem;

        for(var index =_currPosition; index < _content.length && index - _currPosition < _visibleLength; ++index) 
        {
            listItem = document.createElement("li");
			listItem["index"] = index;
            listItem.appendChild(_content[index].item.cloneNode(true));

            _htmlList.appendChild(listItem);
        }
    }

    function updateMaxPosition() 
    {
        _maxPosition = _content.length - _visibleLength;

        if(_maxPosition < 1)
            _maxPosition = 1;
    }

    function updateCurrentPosition()
    {
        if(_currPosition > _maxPosition)
            _currPosition = _maxPosition;
    }

    /**
     * @param {HTMLElement} inParent
     * @returns {LinearScrollList}
     */
    this.setHTMLParent =
        function(inParent) 
        {
            if(_parent)
                _parent.removeChild(_htmlList);

            inParent.appendChild(_htmlList);

            return this;
        }

    this.setVisibleLength =
        function(inNewVisibleLength)
        {
            if(inNewVisibleLength < 0)
                inNewVisibleLength = 0;
            else
            {
                if(inNewVisibleLength > _content.length)
                    inNewVisibleLength = _content.length;
            }

            _visibleLength = inNewVisibleLength;

            updateMaxPosition();
            updateRenderList();
        }

    /**
     * @returns {number}
     */
    this.getVisibleLength =
        function()
        {
            return _visibleLength;
        }

    this.getLength =
        function()
        {
            return _content.length;
        }

    this.clearList =
        function() 
        {
            _content = [];
            _maxPosition = 0;
            _currPosition = 0;

            _htmlList.innerHTML = "";
        }

    /**
     * @param {HTMLElement} inItem
     * @returns {number}
     */
    this.pushListItem =
        function(inItem)
        {
			var item = 
			{
				index : _content.length, 
				item : inItem
			};
			
            var result = _content.push(item);

            updateMaxPosition();
            updateRenderList();

            return result;
        }

    /**
     * @param {HTMLElement} inItem
     * @returns {number}
     */
    this.unshiftListItem =
        function(inItem) 
        {
            var result = _content.unshift(inItem);

            updateMaxPosition();
            updateRenderList();

            return result;
        }

    /**
     * @param {number} inStartIndex
     * @param {number} inDeleteCount
     * @returns {HTMLElement[]}
     */
    this.spliceListItem =
        function(inStartIndex, inDeleteCount)
        {
            var result = _content.splice(arguments);
            updateMaxPosition();

            if(_currPosition > _maxPosition)
                _currPosition = _maxPosition;

            updateRenderList();

            return result;
        }

    /**
     * @typedef {function(HTMLElement, number) => boolean} ForEachCallback
     * @param {ForEachCallback} inCallback
     */
    this.forEach =
        function(inCallback)
        {
            for(var index = 0; index < _content.length; ++index)
            {
                if(!inCallback(_content[index].item, index))
                    break;
            }
        }

    /**
     * @param {any} inEvent
     * @returns {Element}
     */
    function getEventTargetItem(inEvent)
    {
        var target = GetEventTarget(inEvent);

        if(target.id == _htmlList.id)
            return target;

        while(target && target.parentElement.id != _htmlList.id)
            target = target.parentElement;

        return target;
    }

    /**
     * @returns {CSSStyleDeclaration}
     */
    this.style =
        function() 
        {
            return _htmlList.style;
        }

    /**
     * Additional listener types:
     *  - item_selected
     *  - item_unselected
     *  
     * @param {string} eventType
     * @param {EventHandler} callBack
     * @param {boolean} capture
     * @returns {LinearScrollList}
     */
    this.addListEventListener =
        function(eventType, callBack)
        {
            for(var prop in _custom_event_handlers)
            {
                if(_custom_event_handlers.hasOwnProperty(prop) && prop == eventType) 
                {
                    _custom_event_handlers[prop] = callBack;

                    return this;
                }
            }

            BindEvent(_htmlList, eventType, function(inEvent)
            {
				var target = _content[getEventTargetItem(inEvent).index];
                return callBack(inEvent, target.item, target.index);
            });

            return this;
        }

    this.setListPosition =
        function(inIndex)
        {
			if(inIndex < _maxPosition && inIndex >= 0)
			{
				_currPosition = inIndex;
				updateRenderList();
			}
        }
		
	this.getListPosition =
		function()
		{
			return _currPosition;
		}
	
	/**
	 * Doing scroll on "count" elements.
	 * (if count is negative - then scroll 
	 * moving to negative direction)
	 *
	 * @param {number} count
	 */
	this.doScroll =
		function(count)
		{
			var newPos = _currPosition + count;
			
			if(newPos < _maxPosition && newPos >= 0)
			{
				_currPosition = newPos;
				updateRenderList();
			}
		}
		
	this.getSelectedItem =
		function()
		{
			return _selectedItem;
		}
		
	this.selectItem =
		function(index, process_messages)
		{
			if(index >= 0 && index < _content.length)
			{
				if(process_messages)
				{
					if(_selectedItem && _custom_event_handlers.item_unselected)
						_custom_event_handlers.item_unselected.apply(_LinearScrollList, [null, _selectedItem.item, _selectedItem.index]);
					
					_selectedItem = _content[index];
					
					if(_custom_event_handlers.mousedown)
						_custom_event_handlers.mousedown.apply(_LinearScrollList, [null, _selectedItem.item, _selectedItem.index]);

					if(_selectedItem && _custom_event_handlers.item_selected)
						_custom_event_handlers.item_selected.apply(_LinearScrollList, [null, _selectedItem.item, _selectedItem.index]);
					
					return;
				}
				
				_selectedItem = _content[index];
			}
		}
		
    //***       default handlers assign
    //***       (all of them [those which are possible] defined for bubble stage)

    BindEvent(_htmlList, "wheel", function(inEvent) 
    {
        var delta = inEvent.deltaY;

        if(delta < 0)
        {
            if(_currPosition < _maxPosition)
                ++_currPosition;
        }
        else
        {
            if(_currPosition > 0)
                --_currPosition;
        }

		var item = getEventTargetItem(inEvent);
		
        if(_custom_event_handlers.wheel)
            _custom_event_handlers.wheel.apply(_LinearScrollList, [inEvent, _content[item.index], item.index]);

        updateRenderList();
    });

    BindEvent(_htmlList, "mousedown", function(inEvent) 
    {
        if(_selectedItem && _custom_event_handlers.item_unselected)
            _custom_event_handlers.item_unselected.apply(_LinearScrollList, [inEvent, _selectedItem.item, _selectedItem.index]);
		
		_selectedItem = _content[getEventTargetItem(inEvent).index];
		
		if(_custom_event_handlers.mousedown)
            _custom_event_handlers.mousedown.apply(_LinearScrollList, [inEvent, _selectedItem.item, _selectedItem.index]);

        if(_selectedItem && _custom_event_handlers.item_selected)
            _custom_event_handlers.item_selected.apply(_LinearScrollList, [inEvent, _selectedItem.item, _selectedItem.index]);

        updateRenderList();
    });

    BindEvent(_htmlList, "mouseup", function(inEvent) 
    {
		var target = _content[getEventTargetItem(inEvent).index];
		
        if(_custom_event_handlers.mouseup)
            _custom_event_handlers.mouseup.apply(_LinearScrollList, [inEvent, target.item, target.index]);

        updateRenderList();
    });

    //***       style configuring

    _htmlList.style.listStyleType = "none";
    _htmlList.style.padding = "0";
    _htmlList.style.margin = "0";

    //***       unique Id required for correct events handling

	if(inId)
		_htmlList.id = inId;

    //***       input content assigning

    updateMaxPosition();
    updateRenderList();

    //***

    return this;
}