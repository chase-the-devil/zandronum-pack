//***************************
//***   Made by Mr-Cas    ***
//***************************

var C_STRING_LITERALS = 0x01;
var COMMENTS_AFTER_PARAMS = 0x02;

function readIni(path, flags)
{
	var fso = new ActiveXObject("Scripting.FileSystemObject");
	var file = fso.OpenTextFile(path, 1);
	var stringData = file.ReadAll();
	
	file.Close();
	file = undefined;
	
	var ini = 
	{
		global:
		{
			count : 0,
			items : {}
		},
		sections : 
		{
			count : 0,
			items : {}
		}
	};
	
	var currentSection = "";
	
	var index = 0;
	var length = stringData.length;
	var sym = undefined;
	
	while(index < length)
	{
		sym = stringData.charAt(index);
		
		while(index < length && (sym == " " || sym == "\t" || sym == "\r" || sym == "\n"))
			sym = stringData.charAt(++index);
		
		if(index < length && (sym == ";" || sym == "#"))
		{
			while(sym != "\n")
				sym = stringData.charAt(++index);
			
			continue;
		}
		
		if(sym == "[")
		{
			currentSection = "";
			
			++index;
			sym = stringData.charAt(index);
			
			while(index < length && sym != "]")
			{
				currentSection += sym;
				sym = stringData.charAt(++index);
			}
			
			++index;
			
			if(!ini.sections.items[currentSection])
				++ini.sections.count;
		
			ini.sections.items[currentSection] = 
			{
				count : 0,
				items : {}
			};
			
			continue;
		}
		
		var key = "";
		var value = "";
		
		while(index < length && sym != " " && sym != "\t" && sym != "=")
		{
			key += sym;
			sym = stringData.charAt(++index);
		}
		
		while(index < length && sym != "=")
			sym = stringData.charAt(++index);
		
		sym = stringData.charAt(++index);
		
		while(index < length && (sym == " " || sym == "\t"))
			sym = stringData.charAt(++index);
		
		if(sym == "\"" || sym == "'")
		{
			var separator = sym;
		
			value += sym;
			
			sym = stringData.charAt(++index);
			
			if(flags & C_STRING_LITERALS)
			{
				while(index < length && sym != "\r" && sym != "\n" && sym != separator)
				{
					if(sym == "\\")
					{
						value += sym;
						sym = stringData.charAt(++index);
						
						if(index >= length || sym == "\r" || sym == "\n")
							break;
					}
					
					value += sym;
					sym = stringData.charAt(++index);
				}
			}
			else
			{
				while(index < length && sym != "\r" && sym != "\n" && sym != separator)
				{
					value += sym;
					sym = stringData.charAt(++index);
				}
			}
			
			value += sym;
			++index;
		}
		else
		{
			if(flags & COMMENTS_AFTER_PARAMS)
			{
				while(index < length && sym != "\r" && sym != "\n" && sym != ";" && sym != "#")
				{
					value += sym;
					sym = stringData.charAt(++index);
				}
			}
			else
			{
				while(index < length && sym != "\r" && sym != "\n")
				{
					value += sym;
					sym = stringData.charAt(++index);
				}
			}
			
			var last = value.length - 1;
			
			while(value.charAt(last) == " ")
				--last;
				
			if(last < value.length)
				value = value.substring(0, last + 1);
		}
		
		if(index < length)
		{
			if(currentSection == "")
			{
				ini.global.items[key] = value;
				++ini.global.count;
			}
			else
			{
				if(!ini.sections.items[currentSection].items)
					ini.sections.items[currentSection] = 
					{
						count : 0, 
						items : {}
					};
				
				ini.sections.items[currentSection].items[key] = value;
				
				++ini.sections.items[currentSection].count;
			}
		}
	}
	
	return ini;
}

function writeIni(path, ini)
{
	var fso = new ActiveXObject("Scripting.FileSystemObject");
	var file = fso.CreateTextFile(path, true, false);
	
	for(var key in ini.global.items)
		file.WriteLine(key + "=" + ini.global.items[key]);
	
	for(var section in ini.sections.items)
	{
		file.WriteLine("[" + section + "]");
	
		var currSection = ini.sections.items[section];
	
		for(var key in currSection.items)
			file.WriteLine(key + "=" + currSection.items[key]);
	}
	
	file.Close();
	file = undefined;
}