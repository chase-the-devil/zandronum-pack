//***************************
//***   Made by Mr-Cas    ***
//***************************

function ADOBinaryStreamManager()
{	
	var _codePageTable866 		= [];
	var _revCodePageTable866 	= [];
	
	//Local initialization section
	//(To not keep _extendedTable in memory)
	(function()
	{
		//cp866 codepage extended table
		var _extendedTable = 
		{
			128:0x0410, 129:0x0411, 130:0x0412, 131:0x0413, 132:0x0414, 133:0x0415, 134:0x0416, 135:0x0417,
			136:0x0418, 137:0x0419, 138:0x041A, 139:0x041B, 140:0x041C, 141:0x041D, 142:0x041E, 143:0x041F,
			144:0x0420, 145:0x0421, 146:0x0422, 147:0x0423, 148:0x0424, 149:0x0425, 150:0x0426, 151:0x0427,
			152:0x0428, 153:0x0429, 154:0x042A, 155:0x042B, 156:0x042C, 157:0x042D, 158:0x042E, 159:0x042F,
			160:0x0430, 161:0x0431, 162:0x0432, 163:0x0433, 164:0x0434, 165:0x0435, 166:0x0436, 167:0x0437,
			168:0x0438, 169:0x0439, 170:0x043A, 171:0x043B, 172:0x043C, 173:0x043D, 174:0x043E, 175:0x043F,
			176:0x2591, 177:0x2592, 178:0x2593, 179:0x2502, 180:0x2524, 181:0x2561, 182:0x2562, 183:0x2556,
			184:0x2555, 185:0x2563, 186:0x2551, 187:0x2557, 188:0x255D, 189:0x255C, 190:0x255B, 191:0x2510,
			192:0x2514, 193:0x2534, 194:0x252C, 195:0x251C, 196:0x2500, 197:0x253C, 198:0x255E, 199:0x255F,
			200:0x255A, 201:0x2554, 202:0x2569, 203:0x2566, 204:0x2560, 205:0x2550, 206:0x256C, 207:0x2567,
			208:0x2568, 209:0x2564, 210:0x2565, 211:0x2559, 212:0x2558, 213:0x2552, 214:0x2553, 215:0x256B,
			216:0x256A, 217:0x2518, 218:0x250C, 219:0x2588, 220:0x2584, 221:0x258C, 222:0x2590, 223:0x2580,
			224:0x0440, 225:0x0441, 226:0x0442, 227:0x0443, 228:0x0444, 229:0x0445, 230:0x0446, 231:0x0447,
			232:0x0448, 233:0x0449, 234:0x044A, 235:0x044B, 236:0x044C, 237:0x044D, 238:0x044E, 239:0x044F,
			240:0x0401, 241:0x0451, 242:0x0404, 243:0x0454, 244:0x0407, 245:0x0457, 246:0x040E, 247:0x045E,
			248:0x00B0, 249:0x2219, 250:0x00B7, 251:0x221A, 252:0x2116, 253:0x00A4, 254:0x25A0, 255:0x00A0
		};
		
		for(var index = 0; index < 128; ++index)
			_codePageTable866[String.fromCharCode(index)] = index;
		
		for(var index = 128; index < 256; ++index)
			_codePageTable866[String.fromCharCode(_extendedTable[index])] = index;
			
		for(var key in _codePageTable866)
			_revCodePageTable866[_codePageTable866[key]] = key;
	})();
	
	/**
	 * @typedef {{stream :ActiveXObject, size :number}} BinaryStream
	 * @param {string} path
	 * @param {number} mode
	 * @returns {BinaryStream}
	 */
	this.openBinary =
		function(path)
		{
			var stream = new ActiveXObject("ADODB.Stream");
			stream.Type = 2;
			stream.CharSet = 'CP866';
			stream.Open();
			
			stream.LoadFromFile(path);
			
			return stream;
		}
		
	/**
	 * @returns {BinaryStream}
	 */
	this.createBinary =
		function()
		{
			var stream = new ActiveXObject("ADODB.Stream");
			stream.Type = 2;
			stream.CharSet = 'CP866';
			stream.Open();
			
			return stream;
		}
		
	/**
	 * @param {BinaryStream} bStream
	 * @param {string} path
	 */
	this.saveBinary =
		function(bStream, path)
		{
			bStream.SaveToFile(path, 2);
		}

	/**
	 * @param {BinaryStream} bStream
	 * @param {number} size
	 */
	this.readBinary =
		function(bStream, size)
		{
			var stringData = bStream.ReadText(size);
			var bytes = [];
			
			for(var index = 0; index < size; ++index) 
				bytes.push(_codePageTable866[stringData.charAt(index)]);
			
			return bytes;
		}
		
	/**
	 * @param {BinaryStream} bStream
	 * @param {number[]} bytes
	 */
	this.writeBinary =
		function(bStream, bytes)
		{
			var stringData = "";
			var length = bytes.length;
			
			for(var index = 0; index < length; ++index) 
				stringData += _revCodePageTable866[bytes[index]];
			
			bStream.WriteText(stringData);
		}

	/**
	 * @param {BinaryStream} bStream
	 */
	this.closeBinary =
		function(bStream)
		{
			bStream.Close();
			bStream = undefined;
		}
		
	return this;
}

/**
 * @param {number[]} bytes
 * @returns {number}
 */
ADOBinaryStreamManager.bytesToInt32 =
	function(bytes)
	{
		var length = bytes.length;
		
		if(length > 0)
		{
			var result = bytes[0];
			var offset = 0;
			
			for(var index = 1; index < length; ++index)
				result |= (bytes[index] << (offset += 8));
			
			return result;
		}
		
		return undefined;
	}

/**
 * @param {number} int32
 * @returns {number[]}
 */
ADOBinaryStreamManager.int32ToBytes =
	function(int32)
	{
		var result = [];
		
		for(var index = 0; index < 4; ++index)
		{
			result.push(int32 & 0xFF);
			int32 >>= 8;
		}
		
		return result;
	}

/**
 * @param {string} data
 */
ADOBinaryStreamManager.stringFromUTF8Array =
	function(data)
	{
		var extraByteMap = [ 1, 1, 1, 1, 2, 2, 3, 0 ];
		var count = data.length;
		var str = "";
	
		for (var index = 0;index < count && data[index] != 0;)
		{
			var ch = data[index++];
	
			if (ch & 0x80)
			{
				var extra = extraByteMap[(ch >> 3) & 0x07];
				
				if (!(ch & 0x40) || !extra || ((index + extra) > count))
				  return null;
	
				ch = ch & (0x3F >> extra);
				for (; extra > 0; extra -= 1)
				{
					var chx = data[index++];
					if ((chx & 0xC0) != 0x80)
					return null;
	
					ch = (ch << 6) | (chx & 0x3F);
				}
			}
	
			str += String.fromCharCode(ch);
		}

		return str;
	}