//***************************
//***   Made by Mr-Cas    ***
//***************************

/**
 * @param {string} path
 */
function fixFolderPath(path)
{
	path = path.replace("\\", "/");
		
	return (path.charAt(path.length - 1) != "/" ? path + "/" : path);
}

/**
 * @param {string} path
 */
function getParentFolder(path)
{
	var index = path.length;
	
	while(--index >= 0)
	{
		var sym = path.charAt(index);
		
		if(sym == "/" || sym == "\\")
			break;
	}
	
	return path.substring(0, index);
}