/**
* @param {Event} inEvent
* @returns {Element}
*/
function GetEventTarget(inEvent)
{
    inEvent = window.event || inEvent;
    return inEvent.target || inEvent.srcElement;
}