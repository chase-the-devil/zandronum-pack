if MsgBox("Are you sure you want to uninstall zandronum-pack?", 262465, "Uninstall of zandronum-pack") = 1 then

	set objFSO = CreateObject("Scripting.FileSystemObject")

	path = CreateObject("Scripting.FileSystemObject").GetParentFolderName(WScript.ScriptFullName)
	
	if objFSO.FileExists(path & "\zandronum.exe") then
		Dim tempPath
		
		set objShell = CreateObject( "WScript.Shell" )
		
		objFSO.DeleteFile path & "\WADManager.hta", true
		objFSO.DeleteFile path & "\zandronum.exe", true
		objFSO.DeleteFile path & "\doom2.wad", true
		objFSO.DeleteFile path & "\fmodex.dll", true
		objFSO.DeleteFile path & "\skulltag_actors.pk3", true
		objFSO.DeleteFile path & "\zandronum.pk3", true
		objFSO.DeleteFile path & "\zandronum-" & objShell.ExpandEnvironmentStrings("%USERNAME%") & ".ini", true
		
		objFSO.DeleteFolder path & "\skins", true
		objFSO.DeleteFolder path & "\Doomseeker", true
		
		tempPath = objShell.ExpandEnvironmentStrings("%APPDATA%") & "\.doomseeker"
	
		objFSO.DeleteFolder tempPath, true
		
		objFSO.DeleteFile WScript.ScriptFullName, true
		
		MsgBox "zandronum-pack succesfully uninstalled!" 
	else
		MsgBox "Could not locate zandronum.exe! Uninstall failed...", 262160, "Uninstall of zandronum-pack"
	end if
end if