function SelectFolder(startFolder, message)
    dim objFolder, objItem, objShell
    
    on error resume next
    
	SelectFolder = vbNull

    set objShell  = CreateObject("Shell.Application")
    set objFolder = objShell.BrowseForFolder(0, message, 0, startFolder)

    if IsObject(objfolder) then 
		SelectFolder = objFolder.Self.Path
	end if
	
    set objFolder = nothing
    set objshell  = nothing
	
    on error goto 0
	
end function

function GetItemsCount(inFolder, Count)
	dim SubFolder, objFile
	
	for each SubFolder in inFolder.SubFolders
	
		for each objFile in SubFolder.Files
			Count = Count + 1
		next
		
		Count = Count + GetItemsCount(SubFolder, 0)
	next
	
	GetItemsCount = Count
	
end function

function CopyContent(fromPath, toPath)
	dim objShell
	dim objFolder

	set objShell = CreateObject("shell.application")
	set objFolder = objShell.NameSpace(toPath)

	if not objFolder is nothing then
		objFolder.CopyHere fromPath
	end if
	
end function

function ReplaceInFile(filePath, oldValue, newValue)
	const ForReading = 1
	const ForWriting = 2

	set objFSO = CreateObject("Scripting.FileSystemObject")
	
	set objFile = objFSO.OpenTextFile(filePath, ForReading)
	strText = objFile.ReadAll
	objFile.Close
	
	strText = Replace(strText, oldValue, newValue)

	set objFile = objFSO.OpenTextFile(filePath, ForWriting)
	objFile.WriteLine strText 
	objFile.Close
	
end function

' Main body

dim tempPath, path, zandronum_path, filesCount, modalResult

set objFSO = CreateObject("Scripting.FileSystemObject")

path = CreateObject("Scripting.FileSystemObject").GetParentFolderName(WScript.ScriptFullName) & "\data"
filesCount = GetItemsCount(objFSO.GetFolder(path), 0)

if MsgBox("Instalation detected " & filesCount & " files to install, would you like to contunie?", 262465, "Install of zandronum pack") = 1 then

	zandronum_path = SelectFolder(17, "Choose folder for zandronum")
	
	if not TypeName(zandronum_path) = "String" then
		 WScript.Quit
	end if
	
	' Install zandronum files
	
	if not objFSO.FolderExists(zandronum_path) then
		objFSO.CreateFolder(zandronum_path)
	end if
	
	call CopyContent(path & "\zandronum\*", zandronum_path)
	call CopyContent(path & "\*.vbs", zandronum_path)
	call CopyContent(path & "\*.hta", zandronum_path)

	' Install client files

	set objShell = CreateObject( "WScript.Shell" )
	
	tempPath = objShell.ExpandEnvironmentStrings("%APPDATA%") & "\.doomseeker"
	
	if not objFSO.FolderExists(tempPath) then
		objFSO.CreateFolder(tempPath)
	else
		if MsgBox("The '.doomseeker' folder was found! Do you want to clean it before installing?", 262465, "Install of zandronum pack") = 1 then
			objFSO.DeleteFile tempPath & "\*", true
			objFSO.DeleteFolder tempPath & "\*", true
		end if
	end if
	
	call CopyContent(path & "\client\.doomseeker\*", tempPath)
	call CopyContent(path & "\client\*.wad", tempPath)
	call CopyContent(path & "\client\*.ini", zandronum_path)
	
	objFSO.GetFile(zandronum_path & "\zandronum-.ini").Name = "zandronum-" & objShell.ExpandEnvironmentStrings("%USERNAME%") & ".ini"
	
	'Install server files
	
	call CopyContent(path & "\server\*.ini", zandronum_path & "\Doomseeker")
	call CopyContent(path & "\server\*.wad", zandronum_path)
	
	call ReplaceInFile(zandronum_path & "\Doomseeker\server.ini", "%PATH%", Replace(tempPath, "\", "/"))
	call ReplaceInFile(zandronum_path & "\Doomseeker\server.ini", "%IWAD_PATH%", Replace(zandronum_path, "\", "/"))
	
	MsgBox "Installing finished!"
end if