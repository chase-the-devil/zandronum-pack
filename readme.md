# zandonum-pack

This is a package of doom2 game for Windows.
It needs several components to be in

*	zandronum (with Doomseeker)
*	wad-s (IWADS, PWADS) and packages (pk3) addons you wish to inject.
*	Windows ADO ActiveX object components 
	(Any Windows after Windows server 2003 have it by default, 
	if you dont have it follow [this](https://www.microsoft.com/ru-ru/download/details.aspx?id=5793) link).

These components can be choosed custom.

## Building package

1) 	Download zandronum and Doomseeker.
2) 	Place Doomseeker inside of zandronum folder.
3) 	Put zandronum folder (name it as "zandronum") 
	inside of data folder of this package.
4)	Download IWAD (main doom wad you wanna run with) 
	and place it in data\server folder of this package.
5)	Dowload PWADS and pk3 addons and place them in 
	data\client\\.doomseeker folder of this package.

6)	Go to data\server\server.ini and modify parameters: iwad, pwads, pwadsOptional.

*	iwad - main game wad.
*	pwads - put %PATH% before each name of wad you wanna add.
*	pwadsOptional - insert as many zeros ("0;") inside as pwads you have.

After that you are done with building package. 
Just transport it to any Windows machine with wscript server and run install.vbs.
To uninstall run uninstall.vbs.

## Package components

*	install.vbs - Visual Basic script to install all package components on Windows machine.
*	uninstall.vbs - Visual Basic script to uninstall all package components from Windows machine.
*	WAD Manager - script based (javascript) WAD manager that should help to install, 
	uninstall wads into a server and client game.